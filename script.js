class Card {
  constructor(name, email, title, body, id) {
    this.title = title;
    this.body = body;
    this.name = name;
    this.email = email;
    this.id = id;
  }

  render() {
    const card = document.createElement("div");
    card.className = "card";

    const button = document.createElement("a");
    button.href = "#";

    card.append(button);

    card.insertAdjacentHTML(
      "beforeend",
      `<div class="card-header">
        <h2>${this.name}</h2>
        <span>${this.email}</span> 
        
        </div>
        <div class="card-body">
        
        <h2>${this.title}</h2>
        
        <p>${this.body}</p>
        </div>`
    );

    document.body.append(card);

    button.addEventListener("click", (e) => {
      e.preventDefault();
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
        method: "DELETE",
      }).then(({ ok }) => {
        if (ok) {
          button.closest(".card").remove();
        }
      });
    });
  }
}

function createCard() {
  fetch("https://ajax.test-danit.com/api/json/users")
    .then((response) => response.json())
    .then((resultName) => {
      fetch("https://ajax.test-danit.com/api/json/posts")
        .then((response) => response.json())
        .then((resultPost) => {
          resultName.forEach(({ name, email, id }) => {
            resultPost.forEach(({ title, body, userId, id: postID }) => {
              if (id === userId) {
                new Card(name, email, title, body, postID).render();
              }
            });
          });
        });
    });
}

createCard();
